package xyz.ottr.lutra;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.Map;
import java.util.UUID;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.vocabulary.XSD;

public enum Frog {
    ;

    public static final String ns = "http://ns.frog.ottr.xyz/0.1#";
    public static final String prefix_frog = "frog";

    public static final String ns_frog_function = "http://ns.frog.ottr.xyz/0.1/function/";
    public static final String prefix_frog_function = "fn";

    public static final String ns_rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";


    public enum TypeURI {
        ;
        public static final String Function = ns + "Function";
        public static final String lambda = ns + "lambda";

        public static final String functionCallName = "functionCall";
        public static final String typeVarsName = "typeVars";
        public static final String typeArgsName = "typeArgs";
        public static final String functionTermName = "functionTerm";

        public static final String functionCall = ns + functionCallName;
        public static final String functionTerm = ns + functionTermName;
        public static final String typeVars = ns + typeVarsName;
        public static final String typeArgs = ns + typeArgsName;
        public static final String nonVariableGeneric = ns + "nonVariableGeneric";

    }

    public enum RDFFrog {
        ;
        //Classes
        public static final Resource Function = getResource(ns + "Function");
        public static final Resource BaseFunction = getResource(ns + "BaseFunction");
        public static final Resource FunctionCall = getResource(ns + TypeURI.functionCallName);
        public static final Resource functionTerm = getResource(ns + TypeURI.functionTermName);
        public static final Resource GenericType = getResource(ns + "GenericType");
        public static final Resource Template = getResource(OTTR.ns + "Template");

        public static final Property index = getProperty(ns + "index");

        //parameter properties
        public static final Property parameter = getProperty(ns + "parameter");
        public static final Property var = getProperty(ns + "var");
        public static final Property parameterType = getProperty(ns + "parameterType");

        //body properties
        public static final Property def = getProperty(ns + "def");

        public static final Property body = getProperty(ns + "body");
        public static final Property of = getProperty(ns + "of");
        public static final Property val = getProperty(ns + "val");
        public static final Property arg = getProperty(ns + "arg");
        public static final Property argType = getProperty(ns + "argType");
        public static final Property argumentType = getProperty(ns + "argumentType");

        public static final Property returnType = getProperty(ns + "returnType");

        //generic properties
        public static final Property typeVars = getProperty(ns + "typeVars");
        public static final Property typeVar = getProperty(ns + "typeVar");
        public static final Property typeArg = getProperty(ns + "typeArg");
        public static final Property subtypeOf = getProperty(ns + "subtypeOf");

        public static final Property rdfType = getProperty(ns_rdf + "type");
        public static final Resource rdfList = getResource(ns_rdf + "List");


        public static final Resource operation = getResource(ns + "operation");
        public static final Property result = getProperty(ns + "result");
        public static final Property rule = getProperty(ns + "rule");
        public static final Property executableFunctionCall = getProperty(ns + "executableFunctionCall");
        public static final Property usedInTemplate = getProperty(ns + "usedInTemplate");

        public static final Property depth = getProperty(ns + "depth");
        public static final Property type = getProperty(ns + "type");
        public static final Property parameterTypes = getProperty(ns + "parameterTypes");
    }

    public static Resource getResource(String uri) {
        return ResourceFactory.createResource(uri);
    }

    public static Property getProperty(String uri) {
        return ResourceFactory.createProperty(uri);
    }

    private static final String idFunctionSplit = "<-ID-FUNCTIONID->";

    private static final String idTemplateSplit = "<--ID-TEMPLATEID->";

    public static String makeUniqueIdForVar(String id, String identifier) {
        return id + idFunctionSplit + identifier;
    }

    public static String getVarNameFromUniqueId(Resource resource) {
        return resource.isAnon() && resource.getId().getLabelString().length() > 0
                ? "?" + resource.getId().getLabelString().split(idFunctionSplit)[0]
                : resource.toString();
    }

    public static String createUniqueTemplateIRI(String templateName) {
        var uuid = UUID.randomUUID().toString();
        return templateName + idTemplateSplit + uuid;
    }

    public static String getTemplateNameFromUniqueId(Resource resource) {
        return resource.toString().split(idTemplateSplit)[0];
    }

    public enum XPathFrog {
        ;
        public static final String operator_ns = "http://ns.frog.ottr.xyz/0.1/operation/";
        public static final String operator_prefix = "op";
        public static final String xPath_functions_ns = "http://www.w3.org/2005/xpath-functions#";
        public static final String xPath_functions_prefix = "fn";
        public static final String xPath_math_ns = "http://www.w3.org/2005/xpath-functions/math#";
        public static final String xPath_math_prefix = "math";
        public static final String special_functions_ns = "http://ns.frog.ottr.xyz/0.1/special-function/";
        public static final String special_functions_prefix = "sffn";
        public static final String xsd_ns = XSD.NS;
        public static final String xsd_prefix = "xsd";

        public static final Map<String, String> operatorsMap = makeOperators();

        private static Map<String, String> makeOperators() {
            return Map.of(
                    operator_ns + "numeric-add", "+",
                    operator_ns + "numeric-subtract", "-",
                    operator_ns + "numeric-multiply", "*",
                    operator_ns + "numeric-divide", "div",
                    operator_ns + "equal", "=",
                    operator_ns + "numeric-greater-than", ">",
                    operator_ns + "and", "and",
                    operator_ns + "or", "or"
            );
        }

        public static PrefixMapping getXPathMapping() {
            PrefixMapping map = PrefixMapping.Factory.create();
            map.withDefaultMappings(OTTR.getDefaultPrefixes());
            map.setNsPrefix(xPath_functions_prefix, xPath_functions_ns);
            map.setNsPrefix(operator_prefix, operator_ns);
            map.setNsPrefix(xPath_math_prefix, xPath_math_ns);
            map.setNsPrefix(special_functions_prefix, special_functions_ns);
            map.setNsPrefix(xsd_prefix, xsd_ns);
            map.lock();
            return map;
        }

        public static Map<String, String> getXPathMap() {
            var map = getXPathMapping().getNsPrefixMap();
            map.remove(operator_ns);
            map.remove(special_functions_ns);
            map.forEach((key, value) -> map.put(key, value.substring(0, value.length() - 1)));
            return map;
        }
    }


}
