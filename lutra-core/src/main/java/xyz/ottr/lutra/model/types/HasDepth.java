package xyz.ottr.lutra.model.types;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;

public interface HasDepth {

    Resource getDepthRepresentation(Model model, String path, String identifier);

    default Resource nextDepth(Model model, String path, Type type, String identifier) {
        if (type instanceof HasDepth) {
            return ((HasDepth) type).getDepthRepresentation(model, path, identifier);
        }
        var typeResource = type.getRDFRepresentation(model, identifier);
        var blank = model.createResource();
        model.add(model.createLiteralStatement(blank, Frog.RDFFrog.depth, path));
        model.add(model.createStatement(blank, Frog.RDFFrog.type, typeResource));
        return blank;
    }
}
