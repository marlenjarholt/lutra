package xyz.ottr.lutra.model.frog;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.Objects;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.model.ModelElement;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Result;

@Getter
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Parameter implements ModelElement {

    private final @NonNull Term term;

    @Builder
    public static Parameter create(String name, Type type, @NonNull Term term) {
        term.setVariable(true);
        term.setType(Objects.requireNonNullElse(type, term.getVariableType()));

        return new Parameter(term);
    }

    @Override
    public Result<?> validate() {
        return Result.of(this);
    }

    @Override
    public String toString(PrefixMapping prefixes) {
        return this.term.toString(prefixes);
    }

    public Type getType() {
        return term.getType();
    }
}
