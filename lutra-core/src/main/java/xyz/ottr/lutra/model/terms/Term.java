package xyz.ottr.lutra.model.terms;

/*-
 * #%L
 * lutra-core
 * %%
 * Copyright (C) 2018 - 2019 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.model.HasApplySubstitution;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.functions.AbstractFunction;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.Result;

public interface Term extends HasApplySubstitution<Term> {

    Object getIdentifier();

    void setVariable(boolean variable);

    boolean isVariable();

    void setType(Type term);

    Type getType();

    /**
     * Returns the Type that the variable Term has as default if no type is given, and is only based on the
     * Term itself, and therefore not usage.
     */
    default Type getVariableType() {
        // The default type of a variable is the same as for a constant term, except that we remove
        // any surrounding LUB. E.g. an IRI variable has default type IRI.
        return getType().removeLUB();
    }

    Optional<Term> unify(Term other);

    static Optional<Term> unify(Term t1, Term t2) {
        return t1.unify(t2).or(() -> t2.unify(t1));
    }

    Term shallowClone();

    String toString(PrefixMapping prefixMapping);

    Resource toRDF(Model model, String identifier);

    default Term termWithFunctionRef(Map<String, AbstractFunction> functionMap, Type paramType) {
        return this;
    }

    default void validateGenerics(MessageHandler msgs, Map<Term, Generic> genericMap) {
    }

    default Supplier<Result<Term>> execute(List<Term> arguments, List<GenericType> genericArguments) {
        return () -> Result.of(this);
    }

    default Result<Term> termWithRightType(Type type) {
        return Result.of(this.shallowClone());
    }

    default Term substitute(Map<Term, Term> parToArg, Map<Term, Type> genericMap) {
        return this;
    }

}
