package xyz.ottr.lutra.model.frog.functions;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import org.apache.jena.irix.IRIs;
import org.apache.jena.vocabulary.XSD;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.ListTerm;
import xyz.ottr.lutra.model.terms.LiteralTerm;
import xyz.ottr.lutra.model.terms.NoneTerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.BasicType;
import xyz.ottr.lutra.system.Result;

public enum SpecialFunctions {
    ;

    public static Result<Term> callOnSpecialFunction(List<Term> terms, String qname,
                                                     Function<List<Term>, Result<Term>> specialFunctionLookup) {
        var qnameList = qname.split(":");
        if (qnameList.length != 2) {
            return Result.error("Special function not defined");
        }
        switch (qnameList[1]) {
            case "isEmpty":
                return isEmpty(terms, specialFunctionLookup);
            case "head":
                return head(terms, specialFunctionLookup);
            case "tail":
                return tail(terms, specialFunctionLookup);
            case "cons":
                return cons(terms, specialFunctionLookup);
            case "termToList":
                return termToList(terms);
            case "if":
                return ifFrog(terms, specialFunctionLookup);
            case "castToIRI":
                return castToIRI(terms, specialFunctionLookup);
            case "isValidIRI":
                return isValidIRI(terms, specialFunctionLookup);
            default:
                return Result.error("Special function " + qnameList[1] + " does not exist");
        }
    }

    public static Result<Term> isEmpty(List<Term> terms, Function<List<Term>, Result<Term>> specialFunctionLookup) {
        if (terms.size() != 1) {
            return Result.error("Special function isEmpty: expects 1 argument");
        }
        var termResult = executeTerm(terms.get(0));

        if (termResult.isEmpty()) {
            return termResult;
        }

        var term = termResult.get();
        var resultOfLookup = specialFunctionLookup.apply(List.of(term));
        if (resultOfLookup != null) {
            return resultOfLookup;
        }

        if (BaseFunction.argumentsContainsNone(List.of(term))) {
            return Result.of(new NoneTerm());
        }

        if (!(term instanceof ListTerm)) {
            return Result.error("Special function isEmpty: only works on lists");
        }
        var list = ((ListTerm) term).asList();
        var booleanString = list.isEmpty()
                ? "true"
                : "false";
        return Result.of(LiteralTerm.createTypedLiteral(booleanString, XSD.xboolean.getURI()));
    }

    public static Result<Term> head(List<Term> terms, Function<List<Term>, Result<Term>> specialFunctionLookup) {
        if (terms.size() != 1) {
            return Result.error("Special function head isEmpty: expects 1 argument");
        }
        var termResult = executeTerm(terms.get(0));
        if (termResult.isEmpty()) {
            return termResult;
        }

        var term = termResult.get();

        var resultOfLookup = specialFunctionLookup.apply(List.of(term));
        if (resultOfLookup != null) {
            return resultOfLookup;
        }

        if (BaseFunction.argumentsContainsNone(List.of(term))) {
            return Result.of(new NoneTerm());
        }

        if (!(term instanceof ListTerm)) {
            return Result.error("Special function head: only works on lists");
        }
        var list = ((ListTerm) term).asList();
        if (list.isEmpty()) {
            return Result.error("Special function head: list cannot be empty");
        }
        return Result.of(list.get(0));
    }

    public static Result<Term> tail(List<Term> terms, Function<List<Term>, Result<Term>> specialFunctionLookup) {
        if (terms.size() != 1) {
            return Result.error("Special function tail isEmpty: expects 1 argument");
        }
        var termResult = executeTerm(terms.get(0));
        if (termResult.isEmpty()) {
            return termResult;
        }

        var term = termResult.get();
        var resultOfLookup = specialFunctionLookup.apply(List.of(term));
        if (resultOfLookup != null) {
            return resultOfLookup;
        }

        if (BaseFunction.argumentsContainsNone(List.of(term))) {
            return Result.of(new NoneTerm());
        }

        if (!(term instanceof ListTerm)) {
            return Result.error("Special function tail: only works on lists");
        }
        var list = ((ListTerm) term).asList();
        if (list.isEmpty()) {
            return Result.error("Special function tail: cannot find tail of empty function");
        }
        return Result.of(new ListTerm(list.subList(1, list.size()), false));
    }

    public static Result<Term> cons(List<Term> terms, Function<List<Term>, Result<Term>> specialFunctionLookup) {
        if (terms.size() != 2) {
            return Result.error("Special function isEmpty: expects 2 argument");
        }
        var termResult1 = executeTerm(terms.get(0));
        if (termResult1.isEmpty()) {
            return termResult1;
        }

        var term1 = termResult1.get();

        var termResult2 = executeTerm(terms.get(1));
        if (termResult2.isEmpty()) {
            return termResult2;
        }

        var term2 = termResult2.get();

        var resultOfLookup = specialFunctionLookup.apply(List.of(term1, term2));
        if (resultOfLookup != null) {
            return resultOfLookup;
        }

        if (BaseFunction.argumentsContainsNone(List.of(term1, term2))) {
            return Result.of(new NoneTerm());
        }

        if (!(term2 instanceof ListTerm)) {
            return Result.error("Special function cons second argument must be a list");
        }
        var list = new ArrayList<>(((ListTerm) term2).asList());
        list.add(0, term1);
        return Result.of(new ListTerm(list));
    }

    public static Result<Term> termToList(List<Term> terms) {
        if (terms.size() != 1) {
            return Result.error("Special function termToList: expects 1 argument");
        }
        return Result.of(new ListTerm(List.of(terms.get(0)), false));
    }

    public static Result<Term> ifFrog(List<Term> terms, Function<List<Term>, Result<Term>> specialFunctionLookup) {
        if (terms.size() != 3) {
            return Result.error("Special function if: expects 3 argument");
        }

        var boolResult = executeTerm(terms.get(0));
        if (boolResult.isEmpty()) {
            return boolResult;
        }

        var bool = boolResult.get();
        var resultOfLookup = specialFunctionLookup.apply(List.of(bool, terms.get(1), terms.get(2)));
        if (resultOfLookup != null) {
            return resultOfLookup;
        }

        if (BaseFunction.argumentsContainsNone(List.of(bool))) {
            return Result.of(new NoneTerm());
        }

        if (!(bool instanceof LiteralTerm)) {
            return Result.error("Special function if: expects the 1 argument to be of type " + XSD.xboolean.getURI());
        }
        var boolLiteral = (LiteralTerm) bool;
        if (!boolLiteral.getType().isSubTypeOf(new BasicType(XSD.xboolean.getURI()))) {
            return Result.error("Special function if: expects the 1 argument to be of type " + XSD.xboolean.getURI());
        }
        var ifResult = boolLiteral.getValue().equals("true")
                ? terms.get(1)
                : terms.get(2);

        return executeTerm(ifResult);
    }

    private static Result<Term> executeTerm(Term term) {
        return term.execute(List.of(), List.of()).get();
    }

    public static Result<Term> castToIRI(List<Term> terms, Function<List<Term>, Result<Term>> specialFunctionLookup) {
        if (terms.size() != 1) {
            return Result.error("Special function stringToIRI: expects 1 argument");
        }
        var termResult = executeTerm(terms.get(0));
        if (termResult.isEmpty()) {
            return termResult;
        }
        var term = termResult.get();
        var resultOfLookup = specialFunctionLookup.apply(List.of(term));
        if (resultOfLookup != null) {
            return resultOfLookup;
        }

        if (BaseFunction.argumentsContainsNone(List.of(term))) {
            return Result.of(new NoneTerm());
        }

        if (!(term instanceof LiteralTerm)) {
            return Result.error("Special function stringToIRI: expects the 1 argument to be subtype of " + XSD.xstring.getURI());
        }
        var literalTerm = (LiteralTerm) term;
        if (!literalTerm.getType().isSubTypeOf(new BasicType(XSD.xstring.getURI()))) {
            return Result.error("Special function stringToIRI: expects the 1 argument to be subtype of " + XSD.xstring.getURI());
        }

        if (!IRIs.check(literalTerm.getValue())) {
            return Result.error("Special function stringToIRI: " + literalTerm.getValue() + " is not a valid IRI");
        }
        return Result.of(new IRITerm(literalTerm.getValue()));
    }

    public static Result<Term> isValidIRI(List<Term> terms, Function<List<Term>, Result<Term>> specialFunctionLookup) {
        if (terms.size() != 1) {
            return Result.error("Special function isValidIRI: expects 1 argument");
        }
        var termResult = executeTerm(terms.get(0));
        if (termResult.isEmpty()) {
            return termResult;
        }
        var term = termResult.get();
        var resultOfLookup = specialFunctionLookup.apply(List.of(term));
        if (resultOfLookup != null) {
            return resultOfLookup;
        }
        if (!(term instanceof LiteralTerm)) {
            return Result.error("Special function isValidIRI: expects the 1 argument to be subtype of" + XSD.xstring.getURI());
        }

        var literalTerm = (LiteralTerm) term;
        if (!literalTerm.getType().isSubTypeOf(new BasicType(XSD.xstring.getURI()))) {
            return Result.error("Special function isValidIRI: expects the 1 argument to be subtype of " + XSD.xstring.getURI());
        }

        var booleanString = IRIs.check(literalTerm.getValue())
                ? "true"
                : "false";
        return Result.of(LiteralTerm.createTypedLiteral(booleanString, XSD.xboolean.getURI()));
    }
}
