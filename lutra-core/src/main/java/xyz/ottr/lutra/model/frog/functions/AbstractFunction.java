package xyz.ottr.lutra.model.frog.functions;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.Getter;
import lombok.NonNull;
import lombok.Singular;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.ModelElement;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.terms.AbstractTerm;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.FunctionType;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.Result;

@Getter
public abstract class AbstractFunction extends AbstractTerm<String> implements ModelElement {

    @NonNull
    private Term iri;

    @NonNull
    @Singular
    private List<Parameter> parameterList;

    @NonNull
    @Singular
    private Type returnType;

    @NonNull
    @Singular
    private List<Generic> genericList;
    private Map<Term, Generic> genericMap;
    private final HashMap<List<Term>, HashMap<Type, Term>> lookupTable;

    public AbstractFunction(Term iri, List<Parameter> parameterList, Type returnType, List<Generic> genericList) {
        super(iri.getIdentifier().toString(), new FunctionType(makeType(parameterList, returnType)));
        this.iri = iri;
        this.parameterList = parameterList;
        this.returnType = returnType;
        this.genericList = genericList == null ? List.of() : genericList;
        genericMap = this.genericList.stream().collect(Collectors.toMap(Generic::getVar, generic -> generic));
        this.lookupTable = new HashMap<>();
    }

    private static List<Type> makeType(List<Parameter> parameterList, Type returnType) {
        var type = parameterList.stream()
                .map(Parameter::getTerm)
                .map(Term::getType)
                .collect(Collectors.toList());
        type.add(returnType);
        return type;
    }

    @Override
    public Result<? extends AbstractFunction> validate() {

        var result = Result.of(this);
        if (!(iri instanceof IRITerm)) {
            result.addError("The function name must be an IRI");
        }
        var duplicateVarNames = getParameterList().stream()
                .collect(Collectors.groupingBy(parameter -> parameter.getTerm().getIdentifier()))
                .entrySet()
                .stream()
                .filter(e -> e.getValue().size() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        if (!duplicateVarNames.isEmpty()) {
            result.addError("Parameter names must be unique. The function contains multiple occurrence "
                    + "of the same variable name" + duplicateVarNames);
        }
        return result;
    }

    @Override
    public int hashCode() {
        return this.iri.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof AbstractFunction && ((AbstractFunction) other).iri.equals(iri);
    }

    public String getIriString() {
        return iri.toString();
    }

    public Map<Term, Term> makeParToArgMap(List<Term> args) {
        return IntStream.range(0, args.size()).boxed()
                .collect(Collectors.toMap(i -> parameterList.get(i).getTerm(), args::get));
    }

    public Map<Term, Type> makeGenericMap(List<GenericType> genericArgs) {
        return IntStream.range(0, genericList.size()).boxed()
                .collect(Collectors.toMap(i -> genericList.get(i).getVar(), genericArgs::get));
    }

    @Override
    public Resource toRDF(Model model, String function) {
        int parameterPlacement = 0;
        Resource functionIri = model.createResource(iri.getIdentifier().toString());
        model.add(model.createStatement(functionIri, Frog.RDFFrog.rdfType, Frog.RDFFrog.Function));
        //model.add(model.createStatement(functionIri, Frog.newRDFFrog.type, getType().getRDFRepresentation(model,this)));
        model.add(model.createStatement(functionIri, Frog.RDFFrog.returnType, returnType.getRDFRepresentation(model, getIriString())));

        for (Parameter parameter : parameterList) {
            Resource parameterBlankNode = model.createResource();
            model.add(model.createStatement(functionIri, Frog.RDFFrog.parameter, parameterBlankNode));
            model.add(model.createLiteralStatement(parameterBlankNode, Frog.RDFFrog.index, parameterPlacement++));
            model.add(model.createStatement(parameterBlankNode, Frog.RDFFrog.parameterType,
                    parameter.getTerm().getType().getRDFRepresentation(model, getIriString())));
            model.add(model.createStatement(parameterBlankNode, Frog.RDFFrog.var, parameter.getTerm().toRDF(model, getIriString())));
        }

        for (Generic generic : genericList) {
            model.add(model.createStatement(functionIri, Frog.RDFFrog.typeVar, generic.toRDF(model, getIriString())));
        }
        return null;
    }

    public void setFunctions(Map<String, AbstractFunction> functions) {
    }

    ;

    //JAVA VALIDATION IMPLEMENTATION
    public void validateArguments(MessageHandler msgs, Map<String, AbstractFunction> functions) {
    }

    public void validateGenericArguments(MessageHandler msgs) {
    }

    public void validateReturnType(MessageHandler msgs) {
    }


    public Map<Term, Type> getGenericToTypeMap(List<GenericType> genericTypes) {
        return IntStream.range(0, genericList.size()).boxed()
                .collect(Collectors.toMap(i -> genericList.get(i).getVar(), i -> genericTypes.get(i).getType()));
    }

    public FunctionType makeNewFunctionTypeFromGenerics(List<GenericType> genericArguments) {
        var types = ((FunctionType) type).getFunctionType();
        var newTypes = types.stream()
                .map(type -> type.replaceGeneric(getGenericToTypeMap(genericArguments)))
                .collect(Collectors.toList());
        return new FunctionType(newTypes);
    }

    @Override
    public Optional<Term> unify(Term other) {
        return Optional.empty();
    }

    @Override
    public String toString() {
        return iri.getIdentifier().toString();
    }


    public void validateGenerics(MessageHandler msgs, List<GenericType> genericArgs, Map<Term, Generic> genericMap) {
        for (int i = 0; i < genericList.size(); i++) {
            var genericArgsType = genericArgs.get(i);
            var genericType = genericList.get(i);

            var type = genericArgsType.hasOptionalType()
                    ? genericArgsType.getOptionalType()
                    : genericMap.get(genericArgsType.getIdentifier()).getSubtypeOf();
            if (!type.isSubTypeOf(genericType.getSubtypeOf())) {
                msgs.add(Message.error("Function  " + iri.getIdentifier() + " expected the generic variable on index "
                        + i + " to be a subtype of " + genericType.getSubtypeOf() + ", but " + type + " is not."));
            }
        }
    }

    public List<Type> getParametersWithReplacedGenerics(List<GenericType> genericTypes) {
        var functionType = (FunctionType) getType();
        var genericToType = getGenericToTypeMap(genericTypes);
        return functionType.getParameterTypes().stream()
                .map(param -> param.replaceGeneric(genericToType))
                .collect(Collectors.toList());
    }

    public Type getReturnTypeWithReplacedGenerics(List<GenericType> genericTypes) {
        return returnType.replaceGeneric(getGenericToTypeMap(genericTypes));
    }

    //LOOKUP TABLE METHODS
    public void addResultToLookupTable(List<Term> arguments, Result<Term> result, Type calculatedType) {
        if (result.isPresent()) {
            if (lookupTable.containsKey(arguments)) {
                lookupTable.get(arguments).put(calculatedType, result.get());
            } else {
                var map = new HashMap<Type, Term>();
                map.put(calculatedType, result.get());
                lookupTable.put(List.copyOf(arguments), map);
            }
        }
    }

    public Result<Term> getResultFromLookupTable(List<Term> arguments, List<GenericType> genericArguments) {
        var typeMap = getLookupTable().get(arguments);
        var calculatedReturnType = getReturnTypeWithReplacedGenerics(genericArguments);
        if (typeMap.containsKey(calculatedReturnType)) {
            return Result.of(typeMap.get(calculatedReturnType));
        }
        var result = typeMap.values().stream().findFirst();
        if (result.isEmpty()) {
            return Result.error("Frog: Memoisation error");
        }
        var resultWithRightType = result.get().termWithRightType(calculatedReturnType);
        addResultToLookupTable(arguments, resultWithRightType, calculatedReturnType);
        return resultWithRightType;
    }
}
