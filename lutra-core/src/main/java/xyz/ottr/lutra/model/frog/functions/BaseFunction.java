package xyz.ottr.lutra.model.frog.functions;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.Getter;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmValue;
import org.apache.jena.vocabulary.XSD;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.LiteralTerm;
import xyz.ottr.lutra.model.terms.NoneTerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.BasicType;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.model.types.TypeRegistry;
import xyz.ottr.lutra.system.Result;

@Getter
public class BaseFunction extends AbstractFunction {

    private final Term rule;


    public BaseFunction(Term iri, List<Parameter> parameterList, Type returnType, Term rule, List<Generic> genericList) {
        super(iri, parameterList, returnType, genericList);
        this.rule = rule;
    }

    @Builder
    public static BaseFunction create(Term iri, List<Parameter> parameterList, Type returnType, Term rule, List<Generic> genericList) {
        return new BaseFunction(iri, parameterList, returnType, rule, genericList);
    }

    @Override
    public Result<BaseFunction> validate() {
        var result = super.validate();
        if (!(rule instanceof IRITerm)) {
            result.addError("A base functions rules must be a IRI");
        }
        return result.map(f -> (BaseFunction) f);
    }

    @Override
    public Term shallowClone() {
        return new BaseFunction(getIri(), getParameterList(), getReturnType(), rule, getGenericList());
    }

    @Override
    public String toString() {
        return getIriString();
    }

    @Override
    public Supplier<Result<Term>> execute(List<Term> arguments, List<GenericType> genericArguments) {
        return () -> {
            if (getLookupTable().containsKey(arguments)) {
                return getResultFromLookupTable(arguments, genericArguments);
            }

            if (argumentsContainsNone(arguments)) {
                Result<Term> result = Result.of(new NoneTerm());
                addResultToLookupTable(arguments, result, getReturnTypeWithReplacedGenerics(genericArguments));
                return result;
            }

            var qname = Frog.XPathFrog.getXPathMapping().qnameFor(rule.getIdentifier().toString());
            if (qname == null) {
                return Result.error("Base function: Unrecognisable namespace for rule");
            }

            Function<List<Term>, Result<Term>> specialFunctionLookup = (List<Term> evaluatedArguments) -> {
                if (getLookupTable().containsKey(evaluatedArguments)) {
                    return getResultFromLookupTable(evaluatedArguments, genericArguments);
                }
                return null;
            };

            var result = Frog.XPathFrog.special_functions_prefix.equals(getPrefix(qname))
                    ? SpecialFunctions.callOnSpecialFunction(arguments, qname, specialFunctionLookup)
                    : calculateTermWithXpath(arguments, qname, genericArguments);
            addResultToLookupTable(arguments, result, getReturnTypeWithReplacedGenerics(genericArguments));
            return result;
        };
    }

    private Result<Term> calculateTermWithXpath(List<Term> terms, String qname, List<GenericType> genericTypes) {
        var termsExecution = terms.stream()
                .map(term -> term.execute(List.of(), List.of()).get())
                .collect(Collectors.toList());

        var aggr = Result.aggregate(termsExecution);
        if (aggr.isEmpty()) {
            return Result.empty(aggr.getAllMessages());
        }
        var termValues = aggr.get();

        if (getLookupTable().containsKey(termValues)) {
            return getResultFromLookupTable(termValues, genericTypes);
        }

        if (argumentsContainsNone(termValues)) {
            return Result.of(new NoneTerm());
        }

        Result<String> expression;

        switch (getPrefix(qname)) {
            case Frog.XPathFrog.operator_prefix:
                expression = makeExpressionFromOperation(termValues);
                break;
            case Frog.XPathFrog.xPath_functions_prefix:
            case Frog.XPathFrog.xsd_prefix:
                expression = makeExpressionFromFunction(termValues, qname);
                break;
            default:
                expression = Result.error("Base function: Unrecognisable prefix " + getPrefix(qname) + " for xpathFunctions");
                break;
        }
        if (expression.isEmpty()) {
            return Result.empty(expression.getAllMessages());
        }

        var processor = new Processor(false);
        var compiler = processor.newXPathCompiler();
        Frog.XPathFrog.getXPathMap().forEach(compiler::declareNamespace);
        XdmValue value;

        try {
            var selector = compiler.compile(expression.get()).load();
            value = selector.evaluate();
        } catch (SaxonApiException e) {
            return Result.error("Base function: " + e.getLocalizedMessage());
        }

        var resultOptional = value.stream().findFirst();
        var returnValue = ((BasicType) getReturnTypeWithReplacedGenerics(genericTypes)).getIri();
        if (resultOptional.isEmpty()) {
            return Result.error("Execution error in " + getIri().getIdentifier() + " : empty xpath result");
        }
        return checkForXpathSpecialErrors(termValues, resultOptional.get().getStringValue())
                .map(string -> LiteralTerm.createTypedLiteral(string, returnValue))
                .map(term -> (Term) term);
    }

    //Add more when found in the specification to xPath
    private Result<String> checkForXpathSpecialErrors(List<Term> terms, String value) {
        if (value.equals("NaN")) {
            return Result.error("Execution error in " + getIri().getIdentifier()
                    + " : " + terms.get(0) + " cannot be converted to number (NaN error)");
        }
        return Result.of(value);
    }

    private String getPrefix(String qname) {
        return qname.split(":")[0];
    }

    private Result<String> makeExpressionFromOperation(List<Term> terms) {
        var operation = Frog.XPathFrog.operatorsMap.get(rule.getIdentifier().toString());
        if (operation == null) {
            return Result.error("Base function: " + rule.getIdentifier() + " is not an valid operation in frog");
        }

        var builder = new StringBuilder();
        for (Term term : terms) {
            if (!(term instanceof LiteralTerm)) {
                return Result.error("Base function: Operation can only be used on literals");
            }
            var literal = (LiteralTerm) term;
            var value = term.getType().isSubTypeOf(new BasicType(XSD.xboolean.toString()))
                    ? literal.getValue().equals("true") ? "true()" : "false()"
                    : literal.getValue();
            builder.append(value).append(" ").append(operation).append(" ");
        }
        builder.setLength(builder.length() - (operation.length() + 1));
        return Result.of(builder.toString());
    }


    private Result<String> makeExpressionFromFunction(List<Term> terms, String function) {
        var builder = new StringBuilder();
        builder.append(function).append("(");
        for (Term term : terms) {
            if (!(term instanceof LiteralTerm)) {
                return Result.error("Base function: Operation can only be used on literals");
            }
            var literalTerm = (LiteralTerm) term;
            if (literalTerm.getType().isSubTypeOf(new BasicType(XSD.xstring.getURI()))) {
                builder.append("\"").append(literalTerm.getValue()).append("\",");
            } else {
                builder.append(literalTerm.getValue()).append(",");
            }
        }
        builder.setLength(builder.length() - 1);
        builder.append(")");
        return Result.of(builder.toString());
    }

    public static boolean argumentsContainsNone(List<Term> arguments) {
        return arguments.stream().anyMatch(term -> term.getType().equals(TypeRegistry.BOT));
    }

}
