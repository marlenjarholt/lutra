package xyz.ottr.lutra.model.terms;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import lombok.Builder;
import lombok.Getter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.functions.AbstractFunction;
import xyz.ottr.lutra.model.types.FunctionType;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.Result;

@Getter
public class FunctionTerm extends IRITerm {

    private final List<GenericType> genericArguments;
    private AbstractFunction function;

    public FunctionTerm(String iri, List<GenericType> genericArguments) {
        super(iri);
        this.genericArguments = genericArguments == null
                ? List.of()
                : genericArguments;
    }

    public FunctionTerm(AbstractFunction function, Type type, List<GenericType> genericArguments) {
        this(function.getIdentifier(), genericArguments);
        this.function = function;
        this.type = type;
    }

    @Builder
    public static FunctionTerm create(String iri, List<GenericType> genericArguments) {
        return new FunctionTerm(iri, genericArguments);
    }

    @Override
    public Resource toRDF(Model model, String identifier) {
        var resource = model.createResource();
        model.add(model.createStatement(resource, Frog.RDFFrog.rdfType, Frog.RDFFrog.Function));
        model.add(model.createStatement(resource, Frog.RDFFrog.of, super.toRDF(model, identifier)));
        FunctionCall.genericsToRDF(model, resource, identifier, genericArguments);
        return resource;
    }

    @Override
    public Term termWithFunctionRef(Map<String, AbstractFunction> functionMap, Type paramType) {
        if (paramType instanceof FunctionType) {
            var functionRef = functionMap.get(getIdentifier());
            var functionType = (Type) functionRef.makeNewFunctionTypeFromGenerics(genericArguments);
            return new FunctionTerm(functionRef, functionType, this.genericArguments);
        }
        return this;
    }

    @Override
    public void validateGenerics(MessageHandler msgs, Map<Term, Generic> genericMap) {
        function.validateGenerics(msgs, genericArguments, genericMap);
    }

    @Override
    public Supplier<Result<Term>> execute(List<Term> arguments, List<GenericType> genericArguments) {
        return function.execute(arguments, this.genericArguments);
    }
}

