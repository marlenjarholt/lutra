package xyz.ottr.lutra.model.types;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.Getter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.terms.Term;

@Getter
public class FunctionType implements Type, HasDepth {

    private List<Type> functionType;

    @Builder(toBuilder = true)
    public FunctionType(List<Type> functionType) {
        this.functionType = functionType;
    }

    @Override
    public boolean isSubTypeOf(Type other) {
        if (other.equals(TypeRegistry.TOP)) {
            return true;
        }
        if (!(other instanceof FunctionType)) {
            return false;
        }
        var functionTypeOther = (FunctionType) other;
        if (functionTypeOther.functionType.size() != functionType.size()) {
            return false;
        }

        for (int i = 0; i < functionType.size() - 1; i++) {
            if (!functionTypeOther.functionType.get(i).isSubTypeOf(functionType.get(i))) {
                return false;
            }
        }
        var last = functionType.size() - 1;
        return functionType.get(last).isSubTypeOf(functionTypeOther.functionType.get(last));
    }

    public List<Type> getParameterTypes() {
        return functionType.subList(0, functionType.size() - 1);
    }

    public Type getReturnType() {
        return functionType.get(functionType.size() - 1);
    }

    @Override
    public boolean isCompatibleWith(Type other) {
        return isSubTypeOf(other);
    }

    @Override
    public Type removeLUB() {
        return null;
    }

    @Override
    public Resource getRDFRepresentation(Model model, String identifier) {
        return getDepthRepresentation(model, "0", identifier);
    }

    @Override
    public Resource getDepthRepresentation(Model model, String path, String identifier) {
        var resource = model.createResource();
        model.add(model.createStatement(resource, Frog.RDFFrog.rdfType, Frog.RDFFrog.Function));
        model.add(model.createLiteralStatement(resource, Frog.RDFFrog.depth, path));

        for (int i = 0; i < functionType.size(); i++) {
            var newPath = path.length() != 0
                    ? path + "|"
                    : path;
            newPath += i < functionType.size() - 1
                    ? i
                    : "r";
            var type = functionType.get(i);
            var argRec = nextDepth(model, newPath, type, identifier);
            if (i < functionType.size() - 1) {
                model.add(argRec, Frog.RDFFrog.index, model.createTypedLiteral(i));
            }
            model.add(model.createStatement(resource, Frog.RDFFrog.argType, argRec));
        }
        return resource;
    }

    @Override
    public Type replaceGeneric(Map<Term, Type> genericToType) {
        var substitutedType = functionType.stream()
                .map(type -> type.replaceGeneric(genericToType))
                .collect(Collectors.toList());
        return new FunctionType(substitutedType);
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("Function<");
        for (int i = 0; i < functionType.size() - 1; i++) {
            string.append(functionType.get(i).toString()).append(",");
        }
        if (!functionType.isEmpty()) {
            string.append(functionType.get(functionType.size() - 1));
        }
        string.append(">");
        return string.toString();
    }

    @Override
    public int hashCode() {
        return functionType.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof FunctionType)) {
            return false;
        }
        var otherFunction = (FunctionType) other;
        if (otherFunction.functionType.size() != functionType.size()) {
            return false;
        }
        for (int i = 0; i < functionType.size(); i++) {
            if (!functionType.get(i).equals(otherFunction.functionType.get(i))) {
                return false;
            }
        }
        return true;
    }

    public void replaceGenericVariableWithType(Map<Term, Type> genericTermToType) {
        functionType = functionType.stream()
                .map(type -> {
                    if (type instanceof GenericType) {
                        var genericType = (GenericType) type;
                        if (genericTermToType.containsKey(genericType.getIdentifier())) {
                            return genericTermToType.get(genericType.getIdentifier());
                        }
                    }
                    return type;
                })
                .collect(Collectors.toList());
    }

    public FunctionType shallowClone() {
        var ft = toBuilder().build();
        ft.functionType = new ArrayList<>(this.functionType);
        return ft;
    }

    public boolean containGenericType() {
        return functionType.stream().anyMatch(type -> type instanceof GenericType);
    }
}
