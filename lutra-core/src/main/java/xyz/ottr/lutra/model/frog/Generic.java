package xyz.ottr.lutra.model.frog;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.terms.BlankNodeTerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Result;

@Getter
@ToString
public class Generic {

    private @NonNull Term var;
    private @NonNull Type subtypeOf;


    private Generic(Term term, Type subtypeOf) {
        this.var = term;
        this.subtypeOf = subtypeOf;
    }

    @Builder
    public static Generic create(Term term, Type extendType) {
        return new Generic(term, extendType);
    }

    public Result<Generic> validate() {
        var result = Result.of(this);
        if (!(var instanceof BlankNodeTerm)) {
            result.addError("Generic: The generic variable must be a blank node");
        }
        /*if (subtypeOf.containsGeneric()) {
            result.addError("Generic: The extend type can not be or contain generics");
        }*/
        return result;
    }

    public Resource toRDF(Model model, String identifier) {
        var res = model.createResource();
        model.add(model.createStatement(res, Frog.RDFFrog.var, var.toRDF(model, identifier)));
        model.add(model.createStatement(res, Frog.RDFFrog.subtypeOf, subtypeOf.getRDFRepresentation(model, identifier)));
        return res;
    }

    @Override
    public String toString() {
        return var + " generic that is a subtype of " + subtypeOf;
    }

}
