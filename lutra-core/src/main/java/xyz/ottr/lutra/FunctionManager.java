package xyz.ottr.lutra;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.function.Function;
import lombok.Getter;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.io.Format;
import xyz.ottr.lutra.io.FormatManager;
import xyz.ottr.lutra.io.FunctionReader;
import xyz.ottr.lutra.store.FunctionStore;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.Result;

public class FunctionManager {

    private final Settings settings;
    @Getter
    private final PrefixMapping prefixes;
    @Getter
    private final FunctionStore functionStore;
    @Getter
    private final FormatManager formatManager;

    private FunctionManager(Settings settings, PrefixMapping prefixes, FunctionStore functionStore, FormatManager formatManager) {
        this.functionStore = functionStore;
        this.settings = settings;
        this.prefixes = prefixes;
        this.formatManager = formatManager;
    }

    private FunctionManager(FormatManager formatManager, FunctionStore functionStore) {
        this(new Settings(), PrefixMapping.Factory.create().setNsPrefixes(OTTR.getDefaultPrefixes()), functionStore, formatManager);
    }

    public FunctionManager(FunctionStore store) {
        this(store.getFormatManager(), store);
    }

    public FunctionManager(FormatManager formatManager) {
        this(formatManager, makeDefaultStore(formatManager));
    }

    public FunctionManager() {
        this(new FormatManager());
    }


    public static FunctionStore makeDefaultStore(FormatManager formatManager) {
        return new FunctionStore(formatManager);
    }

    public Format getFormat(String formatName) {
        return this.formatManager.getFormat(formatName);
    }

    public void registerFormat(Format format) {
        format.setPrefixMapping(this.prefixes);
        this.formatManager.register(format);
    }

    public MessageHandler readFunctionLibrary(Format format, Collection<String> functions) {
        MessageHandler messages = new MessageHandler();

        for (String fun : functions) {
            Function<FunctionReader, MessageHandler> readerFunction = Files.isDirectory(Paths.get(fun))
                    ? reader -> reader.loadFunctionFromFolder(
                    this.functionStore,
                    fun,
                    this.settings.extensions,
                    this.settings.ignoreExtensions
            )
                    : reader -> reader.loadFunctionFromFile(this.functionStore, fun);
            Result<FunctionReader> reader;

            if (format != null) {
                reader = format.getFunctionReader();
                reader.map(readerFunction).map(messages::combine);
            } else {
                reader = this.functionStore.getFormatManager().attemptAllFrogFormats(readerFunction);
            }
            messages.add(reader);
            reader.ifPresent(r -> this.prefixes.setNsPrefixes(r.getPrefixes()));
        }
        return messages;
    }

    /*public ResultStream<FunctionCall> readFunctionCall(Format format, Collection<String> files){
        Result<FunctionCallReader> reader = format.getFunctionCallReader();
        var fileStream = ResultStream.innerOf(files);

        return reader.mapToStream(fileStream::innerFlatMap);
    }

    public Function<FunctionCall, ResultStream<Term>> makeExecutor(){
        return this.functionStore::executeFunctionCall;
    }*/

    public MessageHandler validateFunctions() {
        return this.functionStore.validateFunctions(prefixes);
    }

    /*public void setFullTrace(boolean enable) {
        this.settings.deepTrace = enable;
        Trace.setDeepTrace(enable);
    }

    public void setStackTrace(boolean enable) {
        this.settings.stackTrace = enable;
        Message.setPrintStackTrace(enable);
    }

    public void setFetchMissingDependencies(boolean enable) {
        this.settings.fetchMissingDependencies = enable;
    }

    /*public void setHaltOn(Message.Severity severity) {
        this.settings.haltOn = severity;
    }*/

    public void setExtensions(String[] ext) {
        this.settings.extensions = ext;
    }

    public void setIgnoreExtensions(String[] ignore) {
        this.settings.ignoreExtensions = ignore;
    }

    static class Settings {

        /*public boolean deepTrace;
        public boolean stackTrace;
        public boolean fetchMissingDependencies;
        public Message.Severity haltOn = Message.Severity.ERROR;*/

        public String[] extensions = { };
        public String[] ignoreExtensions = { };

        //public FormatName fetchFormat; // TODO: Find how to use
    }
}
