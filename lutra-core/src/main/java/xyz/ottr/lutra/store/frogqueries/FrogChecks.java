package xyz.ottr.lutra.store.frogqueries;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import xyz.ottr.lutra.store.frogqueries.quries.CheckFunctionArgumentExists;
import xyz.ottr.lutra.store.frogqueries.quries.CheckFunctionArgumentExistsFunctionCall;
import xyz.ottr.lutra.store.frogqueries.quries.CheckFunctionExists;
import xyz.ottr.lutra.store.frogqueries.quries.CheckFunctionExistsFunctionCall;
import xyz.ottr.lutra.store.frogqueries.quries.CheckGenericVariableExists;
import xyz.ottr.lutra.store.frogqueries.quries.CheckRightAmountOfArguments;
import xyz.ottr.lutra.store.frogqueries.quries.CheckRightAmountOfArgumentsFunctionCall;
import xyz.ottr.lutra.store.frogqueries.quries.CheckRightAmountOfGenericArguments;
import xyz.ottr.lutra.store.frogqueries.quries.CheckRightAmountOfGenericArgumentsFunctionCall;
import xyz.ottr.lutra.store.frogqueries.quries.CheckUnusedGenericParameters;
import xyz.ottr.lutra.store.frogqueries.quries.CheckUnusedParameters;
import xyz.ottr.lutra.store.frogqueries.quries.CheckVariableExists;
import xyz.ottr.lutra.system.Result;

public enum FrogChecks {
    ;

    //FOR FUNCTIONS
    public static final CheckFunctionExists checkFunctionExist = new CheckFunctionExists();
    public static final CheckVariableExists checkVariableExists = new CheckVariableExists();
    public static final CheckGenericVariableExists checkGenericVariableExists = new CheckGenericVariableExists();

    public static final CheckUnusedParameters checkUnusedParameters = new CheckUnusedParameters();
    public static final CheckUnusedGenericParameters checkUnusedGenericParameters = new CheckUnusedGenericParameters();

    public static final CheckRightAmountOfArguments checkCntArgs = new CheckRightAmountOfArguments();
    public static final CheckRightAmountOfGenericArguments checkCntArgsGeneric = new CheckRightAmountOfGenericArguments();
    public static final CheckFunctionArgumentExists checkFunctionArg = new CheckFunctionArgumentExists();
    /*public static final CheckFunctionArgumentNoneGeneric checkFunctionArgNoneGeneric =
            new CheckFunctionArgumentNoneGeneric();*/


    //FOR FUNCTION CALLS
    public static final CheckFunctionExistsFunctionCall CHECK_FUNCTION_EXISTS_FC = new CheckFunctionExistsFunctionCall();
    public static final CheckRightAmountOfArgumentsFunctionCall checkCntArgsFC = new CheckRightAmountOfArgumentsFunctionCall();
    public static final CheckFunctionArgumentExistsFunctionCall checkFunctionArgExistsFC =
            new CheckFunctionArgumentExistsFunctionCall();
    public static final CheckRightAmountOfGenericArgumentsFunctionCall checkCntArgsGenericFC = new
            CheckRightAmountOfGenericArgumentsFunctionCall();

    public static final String folder = "FrogValidationQueries";

    static Result<String> readInQuery(String fileName) {
        var inputStream = FrogChecks.class.getClassLoader().getResourceAsStream(folder + "/" + fileName);
        if (inputStream == null) {
            return Result.error("Failed to read validation file");
        }
        List<String> queryList = new LinkedList<>();

        try {
            queryList.addAll(IOUtils.readLines(inputStream, StandardCharsets.UTF_8));
        } catch (IOException ex) {
            return Result.error(ex.getMessage());
        }
        String query = String.join("\n", queryList);
        return Result.of(query);
    }
}
