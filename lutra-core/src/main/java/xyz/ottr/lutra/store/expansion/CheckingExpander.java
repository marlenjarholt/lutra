package xyz.ottr.lutra.store.expansion;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2021 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import xyz.ottr.lutra.model.Argument;
import xyz.ottr.lutra.model.BaseTemplate;
import xyz.ottr.lutra.model.Instance;
import xyz.ottr.lutra.model.Parameter;
import xyz.ottr.lutra.model.Signature;
import xyz.ottr.lutra.model.Template;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.terms.BlankNodeTerm;
import xyz.ottr.lutra.model.terms.FunctionCall;
import xyz.ottr.lutra.model.terms.FunctionTerm;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.ListTerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.ComplexType;
import xyz.ottr.lutra.model.types.FunctionType;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.LUBType;
import xyz.ottr.lutra.model.types.ListType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.store.TemplateStore;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.system.ResultStream;

public class CheckingExpander extends NonCheckingExpander {

    public CheckingExpander(TemplateStore templateStore) {
        super(templateStore);
    }

    @Override
    public ResultStream<Instance> expandInstance(Instance instance) {
        Result<Instance> errorMessage = checkInstance(instance);
        if (errorMessage.isEmpty()) {
            return ResultStream.of(errorMessage);
        }

        return super.expandInstance(instance);
    }

    private Result<Instance> checkInstance(Instance instance) {
        Result<Signature> signature = getTemplateStore().getSignature(instance.getIri());
        if (signature.isEmpty()) {
            return signature.map(x -> null);
        }
        if (isSignature(signature)) {
            return Result.error("Missing pattern definition of: " + instance.getIri());
        }

        Message error = checkParametersMatch(instance, signature.get());
        if (error != null) {
            return Result.empty(error);
        }

        return Result.of(instance);
    }

    // TODO return messages
    private Message checkParametersMatch(Instance instance, Signature signature) {

        if (instance.getArguments().size() != signature.getParameters().size()) {
            return Message.error("Number of arguments do not match number of paramters in instance " + instance.toString());
        }

        for (int i = 0; i < instance.getArguments().size(); i++) {
            Argument argument = instance.getArguments().get(i);
            Parameter parameter = signature.getParameters().get(i);
            Message error = checkPossibleFunctionArgument(argument, parameter);
            if (error != null) {
                return error;
            }
            error = checkPossibleFunctionCallArgument(argument);
            if (error != null) {
                return error;
            }
            error = checkNonCompatibleArgument(argument, parameter);
            if (error != null) {
                return error;
            }
        }

        return null;
    }

    private Message checkNonCompatibleArgument(Argument argument, Parameter parameter) {

        Type paramType = parameter.getType();
        Type argType = argument.getTerm().getType();

        if (argument.isListExpander()) {
            if (argType instanceof ListType) {
                argType = ((ListType) argType).getInner();
            } else {
                return Message.error("List expander applied to non-list argument: "
                        + argument.toString());
            }
        }

        if (!argType.isCompatibleWith(paramType)) {
            return Message.error("Incompatible argument in instance: "
                    + argument.toString() + " given to parameter "
                    + parameter.toString() + " - incompatible types.");
        }

        if (argument.getTerm() instanceof BlankNodeTerm && parameter.isNonBlank()) {
            return Message.error("Incompatible argument in instance:"
                    + " blank node " + argument.toString() + " given to non-blank"
                    + " parameter " + parameter.toString());
        }

        return null;
    }

    private Message checkPossibleFunctionArgument(Argument argument, Parameter parameter) {
        var paramType = parameter.getType();
        if (paramType instanceof FunctionType
                || paramType instanceof LUBType && ((LUBType) paramType).getInner() instanceof FunctionType) {
            var functionMessages = validateFunction(argument.getTerm());
            if (functionMessages != null) {
                return functionMessages;
            }
            argument.setFunctionRefs(getFunctionMap(), parameter);
        }

        if (paramType instanceof ListType && ((ComplexType) paramType).getInnerMostType() instanceof FunctionType
                && argument.getTerm() instanceof ListTerm) {
            var terms = ((ListTerm) argument.getTerm()).flatMapList();
            for (Term term : terms) {
                var functionMessages = validateFunction(term);
                if (functionMessages != null) {
                    return functionMessages;
                }
            }
            argument.setFunctionRefs(getFunctionMap(), parameter);
        }
        return null;
    }

    private Message validateFunction(Term termArgument) {
        if (!(termArgument instanceof IRITerm) || termArgument.isVariable()) {
            return null; //compatibility test will produce error message instead
        }

        var iriTerm = (IRITerm) termArgument;
        if (!getFunctionMap().containsKey(iriTerm.getIri())) {
            return Message.error("Function not found in instance: the function " + iriTerm.getIri()
                    + " is used in instance but never defined.");
        }

        var function = getFunctionMap().get(iriTerm.getIri());
        var genericArguments = termArgument instanceof FunctionTerm
                ? ((FunctionTerm) termArgument).getGenericArguments()
                : new ArrayList<GenericType>();
        var genericParameters = function.getGenericList();
        var genericSize = checkGenericArgumentsSize(genericParameters, genericArguments, iriTerm.getIri());
        return genericSize != null
                ? genericSize
                : checkGenericArgumentsSubType(genericParameters, genericArguments, iriTerm.getIri());
    }

    private Message checkPossibleFunctionCallArgument(Argument argument) {
        if (!(argument.getTerm() instanceof FunctionCall)) {
            return null;
        }
        var functionCall = (FunctionCall) argument.getTerm();
        var checkFunctionCallFirstStep = checkFunctionCallFirstStep(functionCall);
        if (checkFunctionCallFirstStep != null) {
            return checkFunctionCallFirstStep;
        }
        functionCall.flatMapSetFunction(getFunctionMap(), List.of(), false);
        return checkFunctionCallSecondStep(functionCall);
    }

    private Message checkFunctionCallFirstStep(FunctionCall functionCall) {
        if (!functionCall.isVariable() && !getFunctionMap().containsKey(functionCall.getFunctionCallKey())) {
            return Message.error("function " + functionCall.getFunctionCallKey() + " is not defined");
        }
        var function = getFunctionMap().get(functionCall.getFunctionCallKey());
        var genericParameters = functionCall.isVariable()
                ? new ArrayList<Generic>()
                : function.getGenericList();
        var genericSize = checkGenericArgumentsSize(genericParameters, functionCall.getGenericTypes(),
                functionCall.getFunctionName().toString());
        if (genericSize != null) {
            return genericSize;
        }

        var arguments = functionCall.getArguments();
        var parameters = functionCall.isVariable()
                ? functionCall.getFunctionType().getParameterTypes()
                : function.getParametersWithReplacedGenerics(functionCall.getGenericTypes());
        if (parameters.size() != arguments.size()) {
            return Message.error("Wrong number of arguments in instance: " + function.getIri() + " expects "
                    + function.getParameterList().size() + " but got " + arguments.size()
                    + " arguments.");
        }
        for (Term argument : arguments) {
            if (argument instanceof FunctionCall) {
                var functionCallResult = checkFunctionCallFirstStep((FunctionCall) argument);
                if (functionCallResult != null) {
                    return functionCallResult;
                }
            }
        }
        return null;
    }

    private Message checkFunctionCallSecondStep(FunctionCall functionCall) {
        var function = getFunctionMap().get(functionCall.getFunctionCallKey());
        var genericParameters = functionCall.isVariable()
                ? new ArrayList<Generic>()
                : function.getGenericList();
        var genericArguments = checkGenericArgumentsSize(genericParameters, functionCall.getGenericTypes(),
                functionCall.getFunctionName().toString());
        if (genericArguments != null) {
            return genericArguments;
        }
        var arguments = functionCall.getArguments();
        var parameterList = functionCall.isVariable()
                ? functionCall.getFunctionType().getParameterTypes()
                : function.getParametersWithReplacedGenerics(functionCall.getGenericTypes());
        for (int i = 0; i < parameterList.size(); i++) {
            var paramType = parameterList.get(i);
            var argument = arguments.get(i);
            if (!argument.getType().isCompatibleWith(paramType)) {
                return Message.error("Type error in argument in instance: " + function.getIri() + " expects "
                        + " the generic argument on index " + i + " to be a subtype of " + paramType
                        + " however, the term " + argument + " is not."
                );
            }
            if (argument instanceof FunctionCall) {
                var message = checkFunctionCallSecondStep((FunctionCall) argument);
                if (message != null) {
                    return message;
                }
            }
        }
        return null;
    }

    private Message checkGenericArgumentsSize(List<Generic> genericParameters, List<GenericType> genericArguments,
                                              String functionName) {
        if (genericParameters.size() != genericArguments.size()) {
            return Message.error("Wrong number of generic arguments in instance: " + functionName + " expects "
                    + genericParameters.size() + " but got " + genericArguments.size()
                    + " generic arguments.");
        }
        return null;
    }

    private Message checkGenericArgumentsSubType(List<Generic> genericParameters, List<GenericType> genericArguments,
                                                 String functionName) {
        for (int i = 0; i < genericArguments.size(); i++) {
            var genericArgumentType = genericArguments.get(i).getType();
            var genericParameterType = genericParameters.get(i).getSubtypeOf();
            if (!genericArgumentType.isSubTypeOf(genericParameterType)) {
                return Message.error("Type error in generic argument in instance: " + functionName + " expects "
                        + " the generic argument on index " + i + " to be a subtype of " + genericParameterType
                        + " however, " + genericArgumentType + " is not."
                );
            }
        }
        return null;
    }

    // TODO should go somewhere else where is can be reused
    private boolean isSignature(Result<Signature> signature) {
        return !(signature.get() instanceof Template || signature.get() instanceof BaseTemplate);
    }
}
