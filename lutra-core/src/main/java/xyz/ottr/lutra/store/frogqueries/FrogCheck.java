package xyz.ottr.lutra.store.frogqueries;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.function.Function;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QueryParseException;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.MessageHandler;

public interface FrogCheck extends Function<Model, MessageHandler> {

    String getValidationFile();

    MessageHandler errorMessage(ResultSet resultSet);

    @Override
    default MessageHandler apply(Model model) {
        var queryString = FrogChecks.readInQuery(getValidationFile());
        if (queryString.isEmpty()) {
            var msgs = new MessageHandler();
            msgs.add(queryString);
            return msgs;
        }
        Query query = null;
        try {
            query = QueryFactory.create(queryString.get());
        } catch (QueryParseException ex) {
            var msgs = new MessageHandler();
            msgs.add(Message.error("Error in validation file:\n" + ex.getMessage()));
            return msgs;
        }

        var qe = QueryExecutionFactory.create(query, model);
        var result = qe.execSelect();
        var msgs = errorMessage(result);
        qe.close();
        return msgs;
    }
}
