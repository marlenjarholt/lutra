package xyz.ottr.lutra.io;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xyz.ottr.lutra.model.frog.functions.AbstractFunction;
import xyz.ottr.lutra.model.frog.parser.FunctionParser;
import xyz.ottr.lutra.store.FunctionStore;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.ResultConsumer;
import xyz.ottr.lutra.system.ResultStream;

public class FunctionReader {

    private final Function<String, ResultStream<AbstractFunction>> functionPipeline;
    private final FunctionParser<?> parser; // Needed for retrieving used prefixes
    private static final Logger log = LoggerFactory.getLogger(FunctionReader.class);

    public <M> FunctionReader(InputReader<String, M> functionInputReader,
                              FunctionParser<M> functionParser) {
        this.functionPipeline = ResultStream.innerFlatMapCompose(functionInputReader, functionParser);
        this.parser = functionParser;
    }

    public Map<String, String> getPrefixes() {
        return this.parser.getPrefixes();
    }

    public ResultStream<AbstractFunction> apply(String file) {
        return this.functionPipeline.apply(file);
    }

    public MessageHandler populateFunctionStore(FunctionStore store, String iri) {
        return populateFunctionStore(store, ResultStream.innerOf(iri));
    }

    public MessageHandler populateFunctionStore(FunctionStore store, Set<String> iris) {
        return populateFunctionStore(store, ResultStream.innerOf(iris));
    }

    public MessageHandler populateFunctionStore(FunctionStore store, ResultStream<String> iris) {
        ResultConsumer<AbstractFunction> consumer = new ResultConsumer<AbstractFunction>(store);
        iris.innerFlatMap(this.functionPipeline).forEach(consumer);
        return consumer.getMessageHandler();
    }

    public MessageHandler loadFunctionFromFile(FunctionStore store, String file) {
        ResultConsumer<AbstractFunction> consumer = new ResultConsumer<AbstractFunction>(store);
        this.functionPipeline.apply(file).forEach(consumer);
        return consumer.getMessageHandler();
    }

    /**
     * Loads a folder of templates to be parsed when the parse-method is called.
     *templateParser
     * @param store
     *       the TemplateStore the templates should be loaded into
     * @param folder
     *       the folder containing templates to load
     * @param includeExtensions
     *       the file extensions of the files in the folder to include
     * @param excludeExtensions
     *       the file extensions of the files in the folder to exclude
     * @return
     *       a MessageHandler containing possible Message-s with Warnings, Errors, etc.
     */
    public MessageHandler loadFunctionFromFolder(FunctionStore store, String folder,
                                                 String[] includeExtensions, String[] excludeExtensions) {

        this.log.info("Loading all templates from folder " + folder + " with suffix "
                + Arrays.toString(includeExtensions) + " except " + Arrays.toString(excludeExtensions));

        return populateFunctionStore(store,
                Files.loadFromFolder(folder,
                        includeExtensions,
                        excludeExtensions));
    }

    /**
     * Loads a folder of templates to be parsed when the parse-method is called.
     *
     * @param folder
     *       the folder containing templates to load
     * @param includeExtensions
     *       the file extensions of the files in the folder to include
     * @param excludeExtensions
     *       the file extensions of the files in the folder to exclude
     * @return
     *       a ResultStream containing the parsed TemplateSignatures
     */
    public ResultStream<AbstractFunction> loadFunctionFromFolder(String folder,
                                                                 String[] includeExtensions, String[] excludeExtensions) {
        log.info("Loading all templates from folder " + folder + " with suffix "
                + Arrays.toString(includeExtensions) + " except " + Arrays.toString(excludeExtensions));
        return Files.loadFromFolder(folder, includeExtensions, excludeExtensions)
                .innerFlatMap(this.functionPipeline);
    }

    @Override
    public String toString() {
        return this.parser.toString();
    }
}
