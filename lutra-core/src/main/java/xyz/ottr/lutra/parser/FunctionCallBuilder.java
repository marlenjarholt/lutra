package xyz.ottr.lutra.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import lombok.Builder;
import xyz.ottr.lutra.model.terms.FunctionCall;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.Result;

public enum FunctionCallBuilder {
    ;

    @Builder
    public static Result<Term> create(Result<Term> name, Result<List<Term>> arguments, Result<List<GenericType>> genericTypes) {
        name = Result.nullToEmpty(name, Message.error("A function call needs a name"));
        arguments = Result.nullToEmpty(arguments);
        genericTypes = Result.nullToEmpty(genericTypes);

        var builder = Result.of(FunctionCall.builder());
        builder.addResult(name, FunctionCall.FunctionCallBuilder::functionName);
        builder.addResult(arguments, FunctionCall.FunctionCallBuilder::arguments);
        builder.addResult(genericTypes, FunctionCall.FunctionCallBuilder::genericTypes);

        if (Result.allIsPresent(name, arguments)) {
            return builder.map(FunctionCall.FunctionCallBuilder::build);
        } else {
            return Result.empty(builder);
        }
    }

}
