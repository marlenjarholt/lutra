package xyz.ottr.lutra.frog.hrsfrog.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-frog
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.antlr.v4.runtime.CharStream;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.frog.functions.AbstractFunction;
import xyz.ottr.lutra.model.frog.parser.FunctionBuilder;
import xyz.ottr.lutra.model.frog.parser.FunctionParser;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.stottr.antlr.stOTTRParser;
import xyz.ottr.lutra.stottr.parser.ErrorToMessageListener;
import xyz.ottr.lutra.stottr.parser.SBaseParserVisitor;
import xyz.ottr.lutra.stottr.parser.SParserUtils;
import xyz.ottr.lutra.stottr.parser.SPrefixParser;
import xyz.ottr.lutra.stottr.parser.STermParser;
import xyz.ottr.lutra.stottr.parser.STypeParser;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.system.ResultStream;

public class FFunctionParser extends SBaseParserVisitor<AbstractFunction> implements FunctionParser<CharStream> {

    private final STermParser termParser;
    private final FParameterParser paramParser;
    private final FGenericParameterParser genericParameterParser;
    private Map<String, String> prefixes = new HashMap<>();

    public FFunctionParser() {
        termParser = new STermParser(prefixes, false);
        paramParser = new FParameterParser(termParser);
        genericParameterParser = new FGenericParameterParser(termParser);
    }

    public Map<String, String> getPrefixes() {
        return Collections.unmodifiableMap(this.prefixes);
    }

    public void setPrefixes(Map<String, String> prefixes) {
        this.prefixes = prefixes;
    }

    @Override
    public ResultStream<AbstractFunction> apply(CharStream in) {
        return parseDocument(in);
    }

    private ResultStream<AbstractFunction> parseDocument(CharStream in) {
        ErrorToMessageListener errListener = new ErrorToMessageListener();
        stOTTRParser parser = SParserUtils.makeParser(in, errListener);
        stOTTRParser.FrogDocContext document = parser.frogDoc();

        //Parse prefix
        SPrefixParser prefixParser = new SPrefixParser();
        Result<Map<String, String>> prefixRes = prefixParser.visit(document);

        this.prefixes.putAll(prefixRes.get());

        ResultStream<AbstractFunction> resultStream = prefixRes.mapToStream(_ignore -> {
            Stream<Result<AbstractFunction>> results = document
                    .function()
                    .stream()
                    .map(this::visitFunction);

            return new ResultStream<>(results);
        });

        MessageHandler messageHandler = errListener.getMessageHandler();
        Optional<Message> listenerMessage = messageHandler.toSingleMessage("Parsing Frog.Frog");
        if (listenerMessage.isPresent()) {
            resultStream = ResultStream.concat(ResultStream.of(Result.empty(listenerMessage.get())), resultStream);
        }

        return resultStream;
    }

    public Result<AbstractFunction> visitFunction(stOTTRParser.FunctionContext ctx) {
        if (ctx.functionHead() == null) {
            return Result.error("Parsing function: Function head not found");
        }
        if (ctx.functionBody() == null) {
            return Result.error("Parsing function: Function body not found");
        }

        return FunctionBuilder.builder()
                .iri(parseName(ctx.functionHead()))
                .parameters(parseParameters(ctx.functionHead()))
                .returnType(parseReturnType(ctx.functionHead()))
                .functionCall(parseFunctionBody(ctx))
                .genericList(parseGeneric(ctx.functionHead()))
                .build()
                .map(f -> (AbstractFunction) f);
    }

    private Result<List<Parameter>> parseParameters(stOTTRParser.FunctionHeadContext ctx) {
        if (ctx.frogParameterList() == null) {
            return null;
        }
        var paramList = ctx.frogParameterList().frogParameter().stream()
                .map(this.paramParser::visitParameter)
                .collect(Collectors.toList());
        return Result.aggregate(paramList);
    }

    private Result<Term> parseName(stOTTRParser.FunctionHeadContext ctx) {
        if (ctx.definition() == null || ctx.definition().name() == null || ctx.definition().name().iri() == null) {
            return null;
        }
        return this.termParser.visitIri(ctx.definition().name().iri());
    }

    private Result<Type> parseReturnType(stOTTRParser.FunctionHeadContext ctx) {
        if (ctx.returnType() == null) {
            return null;
        }
        return new STypeParser(termParser, false).visitType(ctx.returnType().type());
    }

    private Result<Term> parseFunctionBody(stOTTRParser.FunctionContext ctx) {
        if (ctx.functionBody() == null || ctx.functionBody().functionCall() == null) {
            return null;
        }
        return this.termParser.visitFunctionCall(ctx.functionBody().functionCall());
    }

    private Result<List<Generic>> parseGeneric(stOTTRParser.FunctionHeadContext ctx) {
        var genericList = ctx.genericParameterList().stream()
                .flatMap(gen -> gen.genericParameter().stream()
                        .map(this.genericParameterParser::visitGeneric)
                ).collect(Collectors.toList());
        return Result.aggregate(genericList);
    }
}
