package xyz.ottr.lutra.frog.rdffrog.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-frog
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
import java.util.List;
import java.util.Map;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.frog.functions.AbstractFunction;
import xyz.ottr.lutra.model.frog.parser.FunctionBuilder;
import xyz.ottr.lutra.model.frog.parser.FunctionParser;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.system.ResultStream;
import xyz.ottr.lutra.wottr.parser.ModelSelector;
import xyz.ottr.lutra.wottr.parser.WTermParser;

public class FFunctionParser implements FunctionParser<Model> {

    private final PrefixMapping prefixes;
    //public static String currentFunctionPrefix;

    public FFunctionParser() {
        this.prefixes = PrefixMapping.Factory.create();
    }

    @Override
    public Map<String, String> getPrefixes() {
        return this.prefixes.getNsPrefixMap();
    }

    @Override
    public ResultStream<AbstractFunction> apply(Model model) {
        this.prefixes.setNsPrefixes(model);
        return ResultStream.innerOf(ModelSelector.getInstancesOfClass(model, Frog.RDFFrog.Function))
                .mapFlatMap(f -> parseFunction(model, f));
    }

    public Result<AbstractFunction> parseFunction(Model model, Resource function) {
        var defList = getDefList(model, function);
        return FunctionBuilder.builder()
                .iri(parseFunctionIRI(function))
                .parameters(parseParameters(model, function, defList))
                .returnType(parseReturnType(model, function))
                .functionCall(parseFunctionBody(defList))
                .genericList(parseGenerics(model, function))
                .build()
                .map(f -> (AbstractFunction) f);
    }

    private Result<RDFList> getDefList(Model model, Resource function) {
        return ModelSelector.getRequiredListObject(model, function, Frog.RDFFrog.def)
                .filterOrMessage(lst -> lst.size() == 3, new Message(Message.Severity.ERROR,
                        "The def list must consist of exactly 3 elements"))
                .filterOrMessage(lst -> lst.get(0).isURIResource(), new Message(Message.Severity.ERROR,
                        "The first element in the def list must be of type IRI"))
                .filterOrMessage(lst -> lst.get(0).asResource().getURI().equals(Frog.TypeURI.lambda), new Message(Message.Severity.ERROR,
                        "The first element in the def list must be the iri " + prefixes.shortForm(Frog.TypeURI.lambda)));
    }

    private Result<Term> parseFunctionIRI(Resource function) {
        //currentFunctionPrefix = RDFNodes.castURIResource(function).get().getURI();
        return WTermParser.toTerm(function);
    }

    private Result<Type> parseReturnType(Model model, Resource function) {
        return ModelSelector.getRequiredResourceObject(model, function, Frog.RDFFrog.type)
                .flatMap(r -> ModelSelector.getRequiredObject(model, r, Frog.RDFFrog.returnType))
                .flatMap(new FTypeParser());
    }

    private Result<List<Parameter>> parseParameters(Model model, Resource function, Result<RDFList> defList) {
        return new FParameterParser(model, function, defList).parseParameters();
    }

    private Result<List<Generic>> parseGenerics(Model model, Resource function) {
        return FGenericListParser.parseGeneric(model, function);
    }

    public Result<Term> parseFunctionBody(Result<RDFList> defList) {
        return defList.map(lst -> lst.get(2))
                .filterOrMessage(node -> node.canAs(RDFList.class), new Message(Message.Severity.ERROR,
                        "The function call must be in a list"))
                .flatMap(WTermParser::toTerm);
    }
}
