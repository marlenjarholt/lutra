package xyz.ottr.lutra.frog.rdffrog;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-frog
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Set;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.frog.rdffrog.parser.FFunctionParser;
import xyz.ottr.lutra.io.Format;
import xyz.ottr.lutra.io.FunctionReader;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.wottr.io.RDFIO;


public class RDFFrog implements Format {

    private static final String name = "RDFFrog";
    private PrefixMapping prefixes;

    private static final Collection<Support> support = Set.of(Support.FunctionReader);

    @Override
    public Result<FunctionReader> getFunctionReader() {
        return Result.of(new FunctionReader(RDFIO.fileReader(), new FFunctionParser()));
    }

    /**
     * Gets the default file suffix for this Format (e.g. ".ttl" for RDF-files).
     */
    public String getDefaultFileSuffix() {
        return ".ttl";
    }

    @Override
    public Collection<Support> getSupport() {
        return support;
    }

    @Override
    public String getFormatName() {
        return name;
    }

    @Override
    public void setPrefixMapping(PrefixMapping prefixes) {
        this.prefixes = prefixes;
    }

    public PrefixMapping getPrefixes() {
        return prefixes;
    }

    /*@Override
    public Result<FunctionCallReader> getFunctionCallReader() {
        return Result.of(new FunctionCallReader(RDFIO.fileReader(), new RDFFunctionCallParser()));
    }*/
}

