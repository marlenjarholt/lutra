package xyz.ottr.lutra.frog.hrsfrog.model;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-frog
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.system.Result;

@Getter
public class Parameters {

    private final Result<List<Parameter>> parameters;

    public Parameters(Result<List<Parameter>> parameters) {
        this.parameters = parameters;
    }

    public Map<String, Term> getVariablesMap() {
        Map<String, Term> variables = new HashMap<>();
        parameters.map(parameters -> {
            parameters.forEach(parameter -> variables.put(parameter.getTerm().getIdentifier().toString(), parameter.getTerm()));
            return parameters;
        });
        return variables;
    }
}
