package xyz.ottr.lutra.frog.rdffrog.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-frog
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
import java.util.List;
import java.util.Map;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.frog.functions.AbstractFunction;
import xyz.ottr.lutra.model.frog.parser.BaseFunctionBuilder;
import xyz.ottr.lutra.model.frog.parser.FunctionParser;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.system.ResultStream;
import xyz.ottr.lutra.wottr.parser.ModelSelector;
import xyz.ottr.lutra.wottr.parser.WTermParser;


public class FBaseFunctionParser implements FunctionParser<Model> {

    private final PrefixMapping prefixes;

    public FBaseFunctionParser() {
        this.prefixes = PrefixMapping.Factory.create();
    }

    public ResultStream<AbstractFunction> apply(Model model) {

        this.prefixes.setNsPrefixes(model);
        return ResultStream.innerOf(ModelSelector.getInstancesOfClass(model, Frog.RDFFrog.BaseFunction))
                .mapFlatMap(f -> parseFunction(model, f));
    }

    public Result<AbstractFunction> parseFunction(Model model, Resource function) {
        return BaseFunctionBuilder.builder()
                .iri(parseFunctionIRI(function))
                .parameters(parseParameters(model, function))
                .returnType(parseReturnType(model, function))
                .rule(parseRule(model, function))
                .genericList(FGenericListParser.parseGeneric(model, function))
                .build()
                .map(f -> (AbstractFunction) f);
    }

    private Result<Term> parseFunctionIRI(Resource function) {
        return WTermParser.toTerm(function);
    }

    private Result<Type> parseReturnType(Model model, Resource function) {
        return ModelSelector.getRequiredResourceObject(model, function, Frog.RDFFrog.type)
                .flatMap(r -> ModelSelector.getRequiredObject(model, r, Frog.RDFFrog.returnType))
                .flatMap(new FTypeParser());
    }

    private Result<List<Parameter>> parseParameters(Model model, Resource function) {
        return new FBaseParameterParser(model, function).parseParameters();
    }

    private Result<Term> parseRule(Model model, Resource function) {
        var value = ModelSelector.getRequiredResourceObject(model, function, Frog.RDFFrog.rule);
        return value.flatMap(WTermParser::toTerm);
    }


    public Map<String, String> getPrefixes() {
        return prefixes.getNsPrefixMap();
    }
}
