package xyz.ottr.lutra.stottr.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-stottr
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.parser.GenericTypeBuilder;
import xyz.ottr.lutra.parser.TermParser;
import xyz.ottr.lutra.stottr.antlr.stOTTRParser;
import xyz.ottr.lutra.system.Result;


public class SParseGenericArgument extends SBaseParserVisitor<GenericType> {

    private STermParser termParser;
    private final boolean stOTTR;

    public SParseGenericArgument(STermParser termParser, boolean stOTTR) {
        this.termParser = termParser;
        this.stOTTR = stOTTR;
    }

    public Result<GenericType> visitGenericArgument(stOTTRParser.GenericArgumentContext ctx) {
        var term = ctx.Variable() != null
                ? termParser.toBlankNodeTerm(termParser.getVariableLabel(ctx.Variable()))
                : TermParser.toTerm(Frog.TypeURI.nonVariableGeneric);
        Result<Type> type = ctx.Variable() != null
                ? null
                : new STypeParser(termParser, stOTTR).visitType(ctx.type());
        return GenericTypeBuilder.builder()
                .term(term)
                .optionalType(type)
                .build();
    }
}
