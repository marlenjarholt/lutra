package xyz.ottr.lutra.wottr.parser;

/*-
 * #%L
 * lutra-wottr
 * %%
 * Copyright (C) 2018 - 2020 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.jena.graph.BlankNodeId;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.FunctionTermBuilder;
import xyz.ottr.lutra.model.terms.BlankNodeTerm;
import xyz.ottr.lutra.model.terms.ListTerm;
import xyz.ottr.lutra.model.terms.LiteralTerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.parser.FunctionCallBuilder;
import xyz.ottr.lutra.parser.GenericTypeBuilder;
import xyz.ottr.lutra.parser.TermParser;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.writer.RDFNodeWriter;

public class WTermParser {

    private static final Map<RDFList, Result<ListTerm>> createdLists = new HashMap<>();

    public static Result<Term> toTerm(RDFNode node) {
        if (node.isResource()) {
            return toTerm(node.asResource());
        } else if (node.isLiteral()) {
            return toLiteralTerm(node.asLiteral()).map(tl -> (Term) tl);
        } else {
            return toError(node);
        }
    }

    public static Result<Term> toTerm(Resource node) {

        if (node.isURIResource()) {
            return TermParser.toTerm(node.getURI());
        } else if (node.canAs(RDFList.class)) {
            return toTerm(node.as(RDFList.class));
        } else if (node.isAnon()) {
            return toBlankNodeTerm(node.getId().getBlankNodeId()).map(tl -> (Term) tl);
        } else {
            return toError(node);
        }
    }

    public static Result<Term> toTerm(RDFList nodeList) {
        List<RDFNode> list = nodeList.asJavaList();
        if (!list.isEmpty() && list.get(0).isURIResource()) {
            var uri = list.get(0).asResource().getURI();
            if (Frog.TypeURI.functionCall.equals(uri)) {
                return toFunctionCall(list);
            }
            if (Frog.TypeURI.functionTerm.equals(uri)) {
                return toFunctionTerm(list);
            }
        }
        return toListTerm(nodeList).map(l -> (Term) l);
    }

    private static Result toError(RDFNode node) {
        return Result.error("Unable to parse " + node.getClass().getSimpleName() + " "
                + RDFNodeWriter.toString(node) + " to Term.");
    }

    private static Result<ListTerm> toListTerm(RDFList list) {

        if (createdLists.containsKey(list)) {
            return createdLists.get(list);
        } else {
            List<Result<Term>> terms = list.asJavaList().stream()
                    .map(WTermParser::toTerm)
                    .collect(Collectors.toList());
            Result<List<Term>> aggTerms = Result.aggregate(terms);
            Result<ListTerm> resTermList = aggTerms.map(ListTerm::new);
            createdLists.put(list, resTermList);
            return resTermList;
        }
    }

    public static Result<LiteralTerm> toLiteralTerm(Literal literal) {
        return TermParser.toLiteralTerm(literal.getLexicalForm(), literal.getDatatypeURI(), literal.getLanguage());
    }

    public static Result<BlankNodeTerm> toBlankNodeTerm(BlankNodeId blankNodeId) {
        return TermParser.toBlankNodeTerm(blankNodeId.getLabelString());
    }

    private static Result<Term> toFunctionCall(List<RDFNode> list) {
        list.remove(0); //removes frog:functionCall
        if (list.isEmpty() || !list.get(0).isResource()) {
            return Result.error("Missing function call name");
        }
        var name = toTerm(list.remove(0).asResource());
        var generics = getGenerics(list);
        var arguments = list.stream()
                .map(WTermParser::toTerm)
                .collect(Collectors.toList());

        return FunctionCallBuilder.builder()
                .name(name)
                .arguments(Result.aggregate(arguments))
                .genericTypes(generics)
                .build();
    }

    private static Result<Term> toFunctionTerm(List<RDFNode> list) {
        list.remove(0); //removes frog:functionTerm
        if (list.isEmpty() || !list.get(0).isResource()) {
            return Result.error("Missing function call name");
        }
        var name = Result.of(list.remove(0).asResource().getURI());
        var genericArguments = getGenerics(list);
        return FunctionTermBuilder.builder()
                .iri(name)
                .genericArguments(genericArguments)
                .build()
                .map(t -> (Term) t);
    }

    private static Result<List<GenericType>> getGenerics(List<RDFNode> list) {
        Result<List<GenericType>> generics = Result.of(List.of());

        if (!list.isEmpty() && list.get(0).canAs(RDFList.class)) {
            var innerList = list.get(0).as(RDFList.class).asJavaList();
            if (!innerList.isEmpty() && innerList.get(0).isURIResource()
                    && Frog.TypeURI.typeArgs.equals(innerList.get(0).asResource().getURI())) {
                innerList.remove(0); //removes the generic list form the list
                list.remove(0); //removes the typeArg tag
                var newGenerics = innerList.stream()
                        .map(term -> {
                            if (term.isAnon()) {
                                return GenericTypeBuilder.builder()
                                        .term(toTerm(term))
                                        .build();
                            }
                            return GenericTypeBuilder.builder()
                                    .term(TermParser.toTerm(Frog.TypeURI.nonVariableGeneric))
                                    .optionalType(new WTypeParser().apply(term))
                                    .build();
                        })
                        .collect(Collectors.toList());
                generics = Result.aggregate(newGenerics);

            }
        }
        return generics;
    }
}
